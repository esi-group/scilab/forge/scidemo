// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function scidemo_plotedolinear ( varargin )
    // Solves a linear Differential Equation.
    //
    // Calling Sequence
    //   scidemo_plotedolinear ( y0 , t0 , t , A )
    //   scidemo_plotedolinear ( y0 , t0 , t , A , rtol )
    //   scidemo_plotedolinear ( y0 , t0 , t , A , rtol , atol )
    //   scidemo_plotedolinear ( y0 , t0 , t , A , rtol , atol , solname )
    //
    // Parameters
    //  y0 : a n-by-1 matrix of doubles, the initial state
    //  t0 : a 1-by-1 matrix of doubles, the initial time
    //  t : a m-by-1 matrix of doubles, the times where to compute the solution
    //  A : a n-by-n real matrix of doubles
    //  rtol : a 1-by-1 matrix of doubles, the relative tolerance (default rtol=1.d-5)
    //  atol : a 1-by-1 matrix of doubles, the absolute tolerance (default atol=1.d-7)
    //  solname : a 1-by-1 matrix of strings, the name of the solver (default solname = "rk"). Solvers available are "adams" "stiff" "rk" "rkf" "fix".
    //  y : a m-by-1 matrix of doubles, the solution of the differential equation
    //
    // Description
    //   Computes the solution of a linear Differential Equation with the 
    //   ode function.
    //
    //   The current function is a demonstration of the following functions.
    //   <itemizedlist>
    //   <listitem>Uses the ode function to compute an approximate solution.</listitem>
    //   <listitem>Uses the expm function to compute the exact solution.</listitem>
    //   </itemizedlist>
    //
    //   We consider the following first order system of linear differential equations:
    //
    //   <latex>
    //   \begin{eqnarray}
    //   \frac{d}{dt} y(t) = Ay(t)
    //   \end{eqnarray}
    //   </latex>
    //
    //   with the initial state:
    //
    //   <latex>
    //   \begin{eqnarray}
    //   y(0) = y_0
    //   \end{eqnarray}
    //   </latex>
    //
    //   The solution of this equation is 
    //
    //   <latex>
    //   \begin{eqnarray}
    //   y(t) =  e^{At} y_0
    //   \end{eqnarray}
    //   </latex>
    //
    // Examples
    //   // To see the source code of this function:
    //   edit scidemo_plotedolinear
    //
    //   // Compute and print the approximate solution.
    //   // The number of time samples.
    //   m = 100;
    //   // The initial state.
    //   y0 = [1;10];
    //   // The linear operator.
    //   A = [  1 1;  0 1.1];
    //   // Initial and final times.
    //   t0 = 0;
    //   tf = 5;
    //   t = linspace(t0,tf,100);
    //   // Make the plot
    //   scf();
    //   scidemo_plotedolinear ( y0 , t0 , t , A )
    //
    //   // Reduce the tolerances
    //   scf();
    //   scidemo_plotedolinear ( y0 , t0 , t , A , 1e-12 , 1e-12 )
    //
    //   // Use another solver
    //   h = scf();
    //   scidemo_plotedolinear ( y0 , t0 , t , A , 1e-5 , 1e-7 , "fix" )
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //
    // Bibliography
    //   http://en.wikipedia.org/wiki/Matrix_exponential
    //   "Syst�mes dynamiques", Claude Gomez, 2007, http://www.scilab.org/team/claude.gomez/papers/sistdynpdf.pdf

    [lhs, rhs] = argn()
    apifun_checkrhs ( "scidemo_plotedolinear" , rhs , 4:7 )
    apifun_checklhs ( "scidemo_plotedolinear" , lhs , 0:1 )
    //
    // Get optional input arguments
    y0 = varargin(1)
    t0 = varargin(2)
    t = varargin(3)
    A = varargin(4)
    if ( rhs <= 4 ) then
      rtol = 1.d-5
    else
      rtol = varargin(5)
    end
    if ( rhs <= 5 ) then
      atol = 1.d-7
    else
      atol = varargin(6)
    end
    if ( rhs <= 6 ) then
      solname = "rk"
    else
      solname = varargin(7)
    end
    //
    // Check Type
    apifun_checktype ( "scidemo_plotedolinear" , y0 , "y0" , 1 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , t0 , "t0" , 2 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , t , "t" , 3 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , A , "A" , 4 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , rtol , "rtol" , 5 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , atol , "atol" , 6 , "constant" )
    apifun_checktype ( "scidemo_plotedolinear" , solname , "solname" , 7 , "string" )
    //
    // Check Size
    nv = size(y0,"*")
    apifun_checkvector ( "scidemo_plotedolinear" , y0 , "y0" , 1 , nv )
    apifun_checkscalar ( "scidemo_plotedolinear" , t0 , "t0" , 2 )
    apifun_checkvector ( "scidemo_plotedolinear" , t , "t" , 3 , size(t,"*") )
    apifun_checkdims ( "scidemo_plotedolinear" , A , "A" , 4 , [nv nv] )
    apifun_checkscalar ( "scidemo_plotedolinear" , rtol , "rtol" , 5 )
    apifun_checkscalar ( "scidemo_plotedolinear" , atol , "atol" , 6 )
    apifun_checkscalar ( "scidemo_plotedolinear" , solname , "solname" , 7 )
    //
    // Check Content
    apifun_checkrange ( "scidemo_normpdf" , rtol , "rtol" , 5 , number_properties("tiniest") , %inf )
    apifun_checkrange ( "scidemo_normpdf" , atol , "atol" , 6 , number_properties("tiniest") , %inf )
    apifun_checkoption ( "scidemo_plotedolinear" , solname , "solname" , 7 , ["adams" "stiff" "rk" "rkf" "fix"] )

    // Compute the solution
    y = scidemo_edolinear ( y0 , t0 , t , A , rtol , atol , solname );
    subplot(2,2,1)
    // Plot y(1)
    plot(t,y(1,:),"bo")
    xtitle("","t (s)","y(1)")
    // Plot y(2)
    subplot(2,2,2)
    plot(t,y(2,:),"bo")
    xtitle("","t (s)","y(2)")

    // Compute and print the exact solution.
    // Uses the expm function to compute the matrix exponential.
    yexact = zeros(2,m);
    for k = 1 : m
        tk = t(k);
        yexact(:,k) = expm(A*tk)*y0;
    end
    subplot(2,2,1)
    plot(t,yexact(1,:),"r-")
    legend(["Approximate" "Exact"]);
    subplot(2,2,2)
    plot(t,yexact(2,:),"r-")
    legend(["Approximate" "Exact"]);

    // Print the relative error
    relerr = abs(yexact-y)./abs(y);
    subplot(2,2,3)
    plot(t,relerr(1,:),"r-")
    xtitle("","t (s)","Relative Error on y(1)")
    subplot(2,2,4)
    plot(t,relerr(2,:),"r-")
    xtitle("","t (s)","Relative Error on y(2)")

endfunction

